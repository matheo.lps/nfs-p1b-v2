﻿
/*
 * Project: NFS-P1B
 * Details: Need For Speed By P1B
 * Authors: Mathéo Lopes, Matéo Piaulenne 
 * Version: V0.1 - 04.11.2022
 */

// Note pour la V300 du projet: Interface graphique 

// Ctrl + k + c
// Ctrl + k + u 

using System;
using System.Runtime.Intrinsics.Arm;
using static System.Net.Mime.MediaTypeNames;

namespace odd_project1
{
    internal class Program
    {
        //========== Declarations 1/2 ==========\\

        public static string[] civic = { "Honda Civic Type-R (2000)", "200 ch", "235 km/h" }; // Civic Type-R
        public static string[] mustang = { "Ford Mustang Foxbody (1990)", "228 ch", "280 km/h" }; // Mustang Foxbody
        public static string[] brz = { "Subaru BRZ Premium (2014)", "200 ch", "226 km/h" }; // BRZ Premium

        public static string[] skyline = { "Nissan Skyline R34 GT-R", "hp", "pwr" };
        public static string[] supra = { "Toyota Supra Mk4", "hp", "pwr" };
        public static string[] rx7 = { "Mazda RX-7 FD", "hp", "pwr" };

        public static string[] r8 = { "Audi R8 V10 Plus", "hp", "pwr" };
        public static string[] huracan = { "Lamborghini Huracan V10", "hp", "pwr" };
        public static string[] italia = { "Ferrari 458 Italia V8", "hp", "pwr" };

        public static List<string> carsTypeBP = new List<string>();

        public static List<string> carsTypePM = new List<string>();

        public static List<string> carsTypeHP = new List<string>();

        public static bool FirstLogin = true;

        public static string PlayerName = "Player";
        public static double PlayerCash = 5000;
        public static int PlayerCarSelected;
        public static string PlayerCar = "";

        public static string choiceDifficulty = "Facile";

        public static int dynamicRandom = 10;

        public static double gainMultiplier;

        public static double bet;

        public static int OptionGarage;

        //========== Variable Random Percentage ==========\\

        public static Random random = new Random();

        static bool CheckIfRaceWin()
        {
            // calcul du taux de réussite (0 - 100)

            // chiffre aléatoire
            const int MAX_PERCENTAGE = 101;
            int rdm = random.Next(1, MAX_PERCENTAGE);

            if (rdm <= dynamicRandom)
            {
                // utilisateur gagne
                return true;
            }
            else
            {
                // Utilisateur perd
                return false;
            }
        }

        static void Main(string[] args)
        {
            //========== Declarations 2/2 ==========\\

            carsTypeBP.Add(civic[0]);
            carsTypeBP.Add(mustang[0]);
            carsTypeBP.Add(brz[0]);

            carsTypePM.Add(skyline[0]);
            carsTypePM.Add(supra[0]);
            carsTypePM.Add(rx7[0]);

            carsTypeHP.Add(r8[0]);
            carsTypeHP.Add(huracan[0]);
            carsTypeHP.Add(italia[0]);

            //========== First Login ==========\\

            if (FirstLogin == true)
            {
                Console.Write("Veuillez saisir un pseudonyme: ");
                PlayerName = Console.ReadLine() ??  "Player";
                if (PlayerName is null || PlayerName == " ")
                {
                    PlayerName = "Player";
                }
                FirstLogin = false;
                Console.Clear();
                Console.WriteLine($"Bienvenue {PlayerName}");
                Console.WriteLine("Veuillez choisir votre première voiture: ");
                for (int i = 0; i < carsTypeBP.Count; i++)
                {
                    Console.WriteLine($"({i + 1})- {carsTypeBP[i]}");
                }
                Console.Write("Voiture N°");
                PlayerCarSelected = Convert.ToInt32(Console.ReadLine());
                PlayerCar = carsTypeBP[PlayerCarSelected - 1];
            }

            //========== Main ==========\\

            while (true)
            {
                DisplayMenu();
            }
        }

        /// <summary>
        /// Display Menu
        /// </summary>
        static void DisplayMenu()
        {
            DisplayHeader();
            Console.WriteLine("MENU:");
            Console.WriteLine();
            Console.WriteLine($"Votre véhicule actuel: {PlayerCar}");
            Console.WriteLine();
            Console.WriteLine($"Selectionnez une option: ");
            Console.WriteLine($"|(1) Garage: ");
            Console.WriteLine($"|(2) Street Race: ");
            Console.WriteLine($"|(3) Quitter le jeu: ");
            Console.Write($"|Option N°");
            int choiceMenu = Convert.ToInt32(Console.ReadLine());
            if (choiceMenu == 1)
            {
                DisplayGarage();
            }
            else if (choiceMenu == 2)
            {
                DisplayRace();
            }
            else if (choiceMenu == 3)
            {
                Environment.Exit(0);
            }
            else
            {
                Console.WriteLine("Erreur");
                Thread.Sleep(2000);
            }
        }

        /// <summary>
        /// Display Garage
        /// </summary>
        static void DisplayGarage()
        {
            DisplayHeader();
            Console.WriteLine("GARAGE:");
            Console.WriteLine();
            Console.WriteLine($"Votre véhicule actuel: {PlayerCar}");
            Console.WriteLine();
            Console.WriteLine("Voitures BP (Basses Performances)");
            for (int i = 0; i < carsTypeBP.Count; i++)
            {
                Console.Write($"{carsTypeBP[i]}, ");
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Voitures PM (Performances Moyennes)");
            for (int i = 0; i < carsTypePM.Count; i++)
            {
                Console.Write($"{carsTypePM[i]}, ");
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Voitures HP (Hautes Performances)");
            for (int i = 0; i < carsTypeHP.Count; i++)
            {
                Console.Write($"{carsTypeHP[i]}, ");
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("|(1) Acheter une voiture");
            Console.WriteLine("|(2) Menu");
            Console.Write($"|Option N°");
            int choiceGarage = Convert.ToInt32(Console.ReadLine());
            if (choiceGarage == 1)
            {
                Console.WriteLine();
                Console.WriteLine("Dans quelle catégorie souhaitez-vous acheter votre voiture ? BP, PM, HP ? (Entrer pour quitter)");
                Console.Write("Catégorie: ");
                string choiceBuy = Console.ReadLine().ToUpper();
                //string choiceBuy = Console.ReadLine().ToUpper() ?? "f";

                if (choiceBuy == "BP")
                {
                    Console.WriteLine("Voitures BP (Basses Performances)");
                    for (int i = 0; i < carsTypeBP.Count; i++)
                    {
                        Console.Write($"{carsTypeBP[i]}, ");
                    }
                }
                else if (choiceBuy == "PM")
                {
                    Console.WriteLine("Voitures PM (Performances Moyennes)");
                    for (int i = 0; i < carsTypePM.Count; i++)
                    {
                        Console.Write($"{carsTypePM[i]}, ");
                    }
                }
                else if (choiceBuy == "HP")
                {
                    Console.WriteLine("Voitures HP (Hautes Performances)");
                    for (int i = 0; i < carsTypeHP.Count; i++)
                    {
                        Console.Write($"{carsTypeHP[i]}, ");
                    }
                }
                // Suite, demander vehicule souahté. 
                else
                {
                    Console.WriteLine("Erreur");
                    Thread.Sleep(2000);
                }
            }
            else { }
        }

        /// <summary>
        /// Display Race
        /// </summary>
        static void DisplayRace()
        {
            DisplayHeader();
            Console.WriteLine("COURSES:");
            Console.WriteLine();
            Console.WriteLine($"Votre véhicule actuel: {PlayerCar}");
            Console.WriteLine();
            Console.WriteLine($"Courses disponibles:");
            Console.WriteLine("|(1) Courir");
            Console.WriteLine("|(2) Menu");
            Console.Write($"|Option N°");
            int choiceRace = Convert.ToInt32(Console.ReadLine());
            if (choiceRace == 1)
            {
                Console.WriteLine();
                Console.Write("Veuillez saisir votre mise: $");
                bet = Convert.ToInt32(Console.ReadLine());
                if (PlayerCash - bet >= 0)
                {
                    PlayerCash = PlayerCash - bet;
                }
                else
                {
                    Console.WriteLine("Erreur");
                    Thread.Sleep(2000);
                }

                Console.WriteLine();
                Console.WriteLine($"|(1) Facile    | {bet} x1.5");
                Console.WriteLine($"|(2) Difficile | {bet} x2");
                Console.WriteLine($"|(3) Extreme   | {bet} x2.5");
                Console.WriteLine("|(4) Menu");
                Console.Write($"|Option N°");
                choiceRace = Convert.ToInt32(Console.ReadLine());
                if (choiceRace == 1)
                {
                    if (carsTypeBP.Contains(PlayerCar))
                    {
                        dynamicRandom = 50;
                    }
                    if (carsTypePM.Contains(PlayerCar))
                    {
                        dynamicRandom = 50;
                    }
                    if (carsTypeHP.Contains(PlayerCar))
                    {
                        dynamicRandom = 50;
                    }
                    choiceDifficulty = "Facile";
                    gainMultiplier = 1.5;
                    DisplayInRace();
                }
                if (choiceRace == 2)
                {
                    if (carsTypeBP.Contains(PlayerCar))
                    {
                        dynamicRandom = 25;
                    }
                    if (carsTypePM.Contains(PlayerCar))
                    {
                        dynamicRandom = 50;
                    }
                    if (carsTypeHP.Contains(PlayerCar))
                    {
                        dynamicRandom = 50;
                    }
                    choiceDifficulty = "Difficile";
                    gainMultiplier = 2;
                    DisplayInRace();
                }
                if (choiceRace == 3)
                {
                    if (carsTypeBP.Contains(PlayerCar))
                    {
                        dynamicRandom = 5;
                    }
                    if (carsTypePM.Contains(PlayerCar))
                    {
                        dynamicRandom = 25;
                    }
                    if (carsTypeHP.Contains(PlayerCar))
                    {
                        dynamicRandom = 50;
                    }
                    choiceDifficulty = "Extreme";
                    gainMultiplier = 2.5;
                    DisplayInRace();
                }
                else if (choiceRace == 4) { }
                else
                {
                    Console.WriteLine("Erreur");
                    Thread.Sleep(2000);
                }
            }
            else if (choiceRace == 2) { }
            else
            {
                Console.WriteLine("Erreur");
                Thread.Sleep(2000);
            }
        }

        /// <summary>
        /// Display In Race
        /// </summary>
        static void DisplayInRace()
        {
            DisplayHeader();
            Console.WriteLine("Street Race:");
            Console.WriteLine();
            Console.WriteLine($"Votre véhicule actuel: {PlayerCar}");
            Console.WriteLine();
            Console.WriteLine($"Difficulté: {choiceDifficulty}");
            Console.WriteLine($"Mise: ${bet}");
            Console.WriteLine($"Multiplicateur de gain: {gainMultiplier}");
            Console.WriteLine();
            Console.Write("Départ dans: ");
            for (int i = 3; i >= 0; i--)
            {
                Thread.Sleep(1000);
                Console.Write($"{i}, ");
            }
            Console.WriteLine();
            Console.WriteLine(dynamicRandom);
            Console.WriteLine();
            bool resulat = CheckIfRaceWin();
            if (resulat)
            {
                double tempResultCash = bet * gainMultiplier;
                PlayerCash = PlayerCash + bet * gainMultiplier;
                Console.WriteLine($"Vous remportez la course! Vous repartez avec ${tempResultCash}");
                Console.WriteLine($"Nouveau Sold En Banque: ${PlayerCash}");
            }
            else
            {
                PlayerCash = PlayerCash - bet;
                Console.WriteLine($"Vous avez perdu cette course, vous perdez votre mise: - ${bet}");
            }
            Console.WriteLine("Appuyez sur une touche pour continuer...");
            Console.ReadKey();
        }

        /// <summary>
        /// Display Header
        /// </summary>
        static void DisplayHeader()
        {
            Console.Clear();
            Console.WriteLine($"|NFS-P1B          |          {PlayerName}          |          ${PlayerCash}");
            Console.WriteLine("|--------------------------------------------------------------------------");
        }

    }
}