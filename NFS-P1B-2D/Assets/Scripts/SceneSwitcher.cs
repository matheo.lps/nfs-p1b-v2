using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    public void ToMain()
    {
        SceneManager.LoadScene(sceneBuildIndex:0);
    }

    public void ToGarage()
    {
        SceneManager.LoadScene("Garage");
    }

    public void ToQGStreetRace()
    {
        SceneManager.LoadScene("QGStreetRace");
    }
}